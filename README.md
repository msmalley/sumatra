# Summary

Sumatra is an experimental java framework I've been toying with, focused on low-level service and domain object management. Given that it is experimental code it's not really ready for use in it's current state

# Features

## Beans

These aren't exactly spring-style beans, in that there is not really an application or lifecycle management. The motivation for this is to provide a generic framework for mapping data to and from java instances. This also allows for dynamic getter/setter methods to include extra parameters for context. For example, let's say you are building a cms and want to include a url for a file object in your system. When serializing this object you will need to know server information like the domain, etc. You essentially have three options:

1. Construct a DAO-style object that maintains an internal session/context which will allow it to retrieve the needed configuration
1. Construct a DTO-style object, and before serialization construct the full url from a code path that has access to the needed data. When processing many rows this could potentially mean iterating those records more than once: first to assign the urls then again to serialize/convert
1. Construct a DTO-style object but provide a getter method for the url that accepts the configuration context as a parameter. This allows the url to be generated during serialization and avoid an extra pass over the data. It also allows for good code organization and consistent OOP patterns but it does need to be supported by the serialization framework.

A simplified example of how that scenario might look:

```
#!java
public class Config {
	public String baseUrl;
}

public class FileObject {
	public String id;
	
	@Getter
	public String getUrl(Config cfg) {
		return cfg.baseUrl + "/files/" + id;
	}
}
```

## Conversion framework

Building on the beans example there is a framework in place to read and write those objects in your format of choice. Actually, in it's current form it focuses on converting the java instances into regular Map<String,Object> which can then be converted to another format. This extra processing may not be ideal for performance-critical situations but sufficient for most use-cases.

## JAX-RS Integration

Providers, etc. for using the conversion and bean utils to read/write params and message bodies.

## Iter

A utility package that adds some functional-like patterns to regular java code. ```Iter<T>``` is at it's simplest a wrapper for a regular ```Iterator<T>``` but adds methods for filtering and map-reduce like operations. Most operations are actually implemented by the various Iterator implementations

## Managers

This isn't terribly well thought out but is meant to provide a framework for reading/writing beans in a database, etc. In theory you would work with DTO-style POJO objects and interact with a manager for persistence. I have tended to prefer this pattern over connected DAOs as the DTOs can then be shared in a client package without the dependencies of the persistence layer. It also can alleviate certain OOP design problems but this is more minor as it really just shifts that burden to the manager hierarchy.

## Patch

There isn't much here yet but the idea is to provide a system for data migrations.

# Roadmap

* General cleanup. It's pretty rough and not really usable now.
* Rethinking of some patterns and features. Perhaps there are more mature tools that some of these problems (spring, jackson, etc.)
* Build out a proper test suite