package org.sumatra.admin;

import java.io.IOException;

import javax.ws.rs.ApplicationPath;

import org.sumatra.admin.resources.ServerStatusResource;
import org.sumatra.web.SumatraApplication;
import org.sumatra.web.resources.SearchResource;

@ApplicationPath("/admin")
public class SumatraAdmin extends SumatraApplication {
    public SumatraAdmin() throws IOException {
        // todo
        register(ServerStatusResource.class);
        register(SearchResource.class);
    }
}
