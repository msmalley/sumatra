package org.sumatra.admin.resources;

import org.sumatra.util.Dictionary;

public class ServerStatusResource implements ServerStatus {
    @Override
    public Dictionary getServerStatus() {
        return Dictionary.builder()
                .set("status", "healthy")
                .set("version", "1.0").build();
    }
}
