package org.sumatra.admin.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.sumatra.util.Dictionary;

@Path("/status")
@Produces(MediaType.APPLICATION_JSON)
public interface ServerStatus {
    @GET
    public Dictionary getServerStatus();
}
