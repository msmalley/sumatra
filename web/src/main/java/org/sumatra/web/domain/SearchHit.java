package org.sumatra.web.domain;

import org.sumatra.domain.BaseObject;

public class SearchHit {
    public double score;
    public BaseObject object;
}
