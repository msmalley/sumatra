package org.sumatra.web.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matt on 1/19/14.
 */
public class SearchResult {
    public long totalCount;
    public List<SearchHit> hits = new ArrayList<>();
}
