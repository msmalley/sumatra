package org.sumatra.web.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.sumatra.web.domain.SearchResult;

@Path("/search")
@Produces(MediaType.APPLICATION_JSON)
public interface Search {
    @GET
    public SearchResult search(@QueryParam("query") String query);
}
