package org.sumatra.web;

import java.io.IOException;
import java.util.Properties;

import org.glassfish.jersey.server.ResourceConfig;
import org.sumatra.util.JsonUtil;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public abstract class SumatraApplication extends ResourceConfig {
    protected Properties config;

    public SumatraApplication() throws IOException {
        register(new JacksonJsonProvider(JsonUtil.getObjectMapper()));
        register(new ParamConverterProvider());

        config = new Properties();
        config.load(getClass().getClassLoader().getResourceAsStream("web.properties"));

        // FIXME
//        String name = config.getProperty("name");
//        String packages = config.getProperty("packages");
//
//        setApplicationName(name);
//        packages(packages);
    }

    public Properties getConfig() {
        return config;
    }
}
