package org.sumatra.web;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Dictionary;

import javax.ws.rs.ext.ParamConverter;

import org.sumatra.util.JsonUtil;

public class ParamConverterProvider implements javax.ws.rs.ext.ParamConverterProvider {
    public static class DictionaryConverter implements ParamConverter<Dictionary> {
        @Override
        public Dictionary fromString(String value) {
            try {
                return JsonUtil.getObjectMapper().readValue(value, Dictionary.class);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public String toString(Dictionary value) {
            try {
                return JsonUtil.getObjectMapper().writeValueAsString(value);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if(Dictionary.class.isAssignableFrom(rawType)) {
            return (ParamConverter<T>) new DictionaryConverter();
        }

        return null;
    }
}
