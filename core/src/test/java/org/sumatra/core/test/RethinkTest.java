package org.sumatra.core.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sumatra.managers.rethinkdb.RethinkDB;
import org.sumatra.util.Dictionary;
import org.sumatra.util.iter.Iter;
import org.sumatra.util.iter.IterMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.dkhenry.RethinkDB.RqlConnection;
import com.dkhenry.RethinkDB.RqlCursor;
import com.dkhenry.RethinkDB.RqlObject;
import com.dkhenry.RethinkDB.errors.RqlDriverException;

public class RethinkTest {
    protected String dbHost = "localhost";
    protected int dbPort = 28015;
    protected String testDb = "test";
    protected String testTable = "testTable";

    @Test
    public void dbList() throws RqlDriverException {
        RethinkDB rql = RethinkDB.connect(dbHost, dbPort);
        RqlConnection connection = rql.connection();

        RqlCursor cursor = connection.run(connection.db_list());
        System.out.println(cursor);
    }

    @Test
    public void tableCreate() throws RqlDriverException {
        RethinkDB rql = RethinkDB.connect(dbHost, dbPort);
        rql.tableCreate(testDb, testTable);
    }

    @Test(dependsOnMethods={"tableCreate"})
    public void tableList() throws RqlDriverException {
        RethinkDB rql = RethinkDB.connect(dbHost, dbPort);
        List<String> tableList = rql.tableList(testDb).list();
        Assert.assertEquals(tableList.get(0), testTable);
    }

    @Test(dependsOnMethods={"tableList"})
    public void insert() throws RqlDriverException {
        Dictionary obj = Dictionary.builder()
                .set("foo", 123.0) // GOTCHA: all numbers converted to double, won't test equals later if not double here
                .set("colors", Arrays.asList("red", "green", "blue"))
                .build();

        RethinkDB rql = RethinkDB.connect(dbHost, dbPort);
        rql.insert(testDb, testTable, obj.toMap());

        List<Dictionary> results = rql.findObjects(testDb, testTable).map(
                new IterMapper<Map<String, Object>, Dictionary>() {
                    @Override
                    public Dictionary map(Map<String, Object> map) {
                        return new Dictionary(map);
                    }
                }).list();
        Assert.assertEquals(results.size(), 1);

        Dictionary result = results.get(0);
        Assert.assertFalse(result.remove("id") == null);
        Assert.assertEquals(result, obj);
    }

    @Test(dependsOnMethods={"insert"}, alwaysRun=true)
    public void tableDrop() throws RqlDriverException {
        RethinkDB rql = RethinkDB.connect(dbHost, dbPort);
        rql.tableDrop(testDb, testTable);

        List<String> tableList = rql.tableList(testDb).list();
        Assert.assertEquals(tableList.size(), 0);
    }
}
