package org.sumatra.core.test.convert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sumatra.util.beans.Getter;
import org.sumatra.util.beans.Ignore;
import org.sumatra.util.convert.Converter;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BasicConverterTest {
    public static class TestBean {
        public String testString;
        public int testInt;
        public TestBean parent;
        public List<TestBean> children = new ArrayList<>();
        public Set<TestBean> children2 = new HashSet<>();
        public Map<String,TestBean> children3 = new HashMap<>();

        @Ignore
        public String ignored;

        @Getter
        public String getDynamicString() {
            return "lorem ipsum";
        }
    }

    protected Converter converter;

    @Test
    public void setup() {
        converter = Converter.standardConverter();
        converter.getBeanConverter().registerType(TestBean.class);
    }

    @Test(dependsOnMethods={"setup"})
    public void testBeanToMap() {
        TestBean bean = new TestBean();
        bean.testString = "foo";
        bean.testInt = 42;
        bean.parent = new TestBean();
        bean.parent.testString = "bar";
        bean.parent.testInt = 11235;
        bean.children.add(new TestBean());
        bean.children.get(0).testString = "baz";
        bean.children2.add(new TestBean());
        bean.children2.iterator().next().testString = "qux";
        bean.children3.put("quux", new TestBean());
        bean.children3.get("quux").testString = "corge";

        Map<String,Object> map = converter.convert(bean, Map.class);

        Assert.assertEquals(map.size(), 8);
        Assert.assertEquals(map.get("_type"), TestBean.class.getName());
        Assert.assertEquals(map.get("testString"), bean.testString);
        Assert.assertEquals(map.get("testInt"), bean.testInt);
        Assert.assertEquals(map.get("dynamicString"), bean.getDynamicString());

        Map<String,Object> parentMap = (Map<String, Object>) map.get("parent");
        Assert.assertEquals(parentMap.size(), 8);
        Assert.assertEquals(parentMap.get("_type"), TestBean.class.getName());
        Assert.assertEquals(parentMap.get("testString"), bean.parent.testString);
        Assert.assertEquals(parentMap.get("testInt"), bean.parent.testInt);
        Assert.assertEquals(parentMap.get("dynamicString"), bean.getDynamicString());
        Assert.assertNull(parentMap.get("parent"));

        List<TestBean> children = (List<TestBean>) map.get("children");
        Assert.assertEquals(children.size(), 1);

        Map<String,Object> childMap = (Map<String, Object>) children.get(0);
        Assert.assertEquals(childMap.size(), 8);
        Assert.assertEquals(childMap.get("_type"), TestBean.class.getName());
        Assert.assertEquals(childMap.get("testString"), bean.children.get(0).testString);
        Assert.assertEquals(childMap.get("testInt"), bean.children.get(0).testInt);
        Assert.assertEquals(childMap.get("dynamicString"), bean.getDynamicString());
        Assert.assertNull(childMap.get("parent"));
        Assert.assertEquals(((List<?>) childMap.get("children")).size(), 0);

        Set<Map<String,Object>> children2 = (Set<Map<String, Object>>) map.get("children2");
        Assert.assertEquals(children2.size(), 1);
        childMap = children2.iterator().next();
        Assert.assertEquals(childMap.size(), 8);
        Assert.assertEquals(childMap.get("testString"), "qux");

        Map<String,Object> children3 = (Map<String, Object>) map.get("children3");
        Assert.assertEquals(children3.size(), 1);
        childMap = (Map<String, Object>) children3.get("quux");
        Assert.assertEquals(childMap.size(), 8);
        Assert.assertEquals(childMap.get("testString"), "corge");
    }

    @Test(dependsOnMethods={"setup"})
    public void testBeanFromMap() {
        Map<String,Object> map = new HashMap<>();
        map.put("_type", TestBean.class.getName());
        map.put("testString", "foo");
        map.put("testInt", 42);
        map.put("ignored", "bar");
        map.put("dyanmicString", "baz");

        // parent

        Map<String,Object> parentMap = new HashMap<>();
        parentMap.put("_type", TestBean.class.getName());
        parentMap.put("testString", "qux");
        parentMap.put("testInt", 11235);
        parentMap.put("ignored", "quux");
        parentMap.put("dyanmicString", "corge");

        map.put("parent", parentMap);

        // children

        Map<String,Object> childMap = new HashMap<>();
        childMap.put("_type", TestBean.class.getName());
        childMap.put("testString", "grault");

        List<Object> children = new ArrayList<>();
        children.add(childMap);

        map.put("children", children);

        // children2

        childMap = new HashMap<>();
        childMap.put("_type", TestBean.class.getName());
        childMap.put("testString", "garply");

        Set<Object> children2 = new HashSet<>();
        children2.add(childMap);

        map.put("children2", children2);

        // children3

        childMap = new HashMap<>();
        childMap.put("_type", TestBean.class.getName());
        childMap.put("testString", "waldo");

        Map<String,Object> children3 = new HashMap<>();
        children3.put("fred", childMap);

        map.put("children3", children3);

        // test fromMap

        TestBean bean = converter.convert(map, TestBean.class);

        Assert.assertTrue(bean.getClass().equals(TestBean.class));
        Assert.assertEquals(bean.testString, "foo");
        Assert.assertEquals(bean.testInt, 42);
        Assert.assertNull(bean.ignored);
        Assert.assertEquals(bean.getDynamicString(), "lorem ipsum");

        // test parent

        Assert.assertTrue(bean.parent.getClass().equals(TestBean.class));
        Assert.assertEquals(bean.parent.testString, "qux");
        Assert.assertEquals(bean.parent.testInt, 11235);
        Assert.assertNull(bean.parent.ignored);
        Assert.assertEquals(bean.parent.getDynamicString(), "lorem ipsum");
        Assert.assertNull(bean.parent.parent);

        // test children

        Assert.assertEquals(bean.children.size(), 1);
        Assert.assertTrue(bean.children.get(0).getClass().equals(TestBean.class));
        Assert.assertEquals(bean.children.get(0).testString, "grault");

        // test children2

        Assert.assertEquals(bean.children2.size(), 1);
        Assert.assertTrue(bean.children2.iterator().next().getClass().equals(TestBean.class));
        Assert.assertEquals(bean.children2.iterator().next().testString, "garply");

        // test children3

        Assert.assertEquals(bean.children3.size(), 1);
        Assert.assertTrue(bean.children3.get("fred").getClass().equals(TestBean.class));
        Assert.assertEquals(bean.children3.get("fred").testString, "waldo");
    }
}
