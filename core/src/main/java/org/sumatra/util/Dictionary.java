package org.sumatra.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Dictionary {
    public static class Builder {
        protected Dictionary dict = new Dictionary();

        public Builder set(String key, Object value) {
            dict.set(key, value);
            return this;
        }

        public Dictionary build() {
            return new Dictionary(dict);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    protected Map<String,Object> map = new HashMap<>();

    public Dictionary() {}

    public Dictionary(Map<String,Object> map) {
        this.map = map;
    }

    public Dictionary(Dictionary other) {
        this(new HashMap<>(other.map));
    }

    @Override
    public boolean equals(Object other) {
        if(other instanceof Dictionary) {
            return ((Dictionary)other).map.equals(map);
        } else if(other instanceof Map) {
            return ((Map)other).equals(map);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    public Set<String> keys() {
        return map.keySet();
    }

    public <T> T get(String key, Class<T> cls) {
        return (T)map.get(key);
    }

    public List<?> getList(String key) {
        return getListOf(key, Object.class);
    }

    public <T> List<T> getListOf(String key, Class<T> cls) {
        return (List<T>)map.get(key);
    }

    public Set<?> getSet(String key) {
        return getSetOf(key, Object.class);
    }

    public <T> Set<T> getSetOf(String key, Class<T> cls) {
        return (Set<T>)map.get(key);
    }

    public Object getObject(String key) {
        return get(key, Object.class);
    }

    public String getString(String key) {
        return get(key, String.class);
    }

    public Integer getInteger(String key) {
        return get(key, Integer.class);
    }

    public Long getLong(String key) {
        return get(key, Long.class);
    }

    public Double getDouble(String key) {
        return get(key, Double.class);
    }

    public Boolean getBoolean(String key) {
        return get(key, Boolean.class);
    }

    public void put(String key, Object value) {
        set(key, value);
    }

    public void set(String key, Object value) {
        map.put(key, value);
    }

    public void setDefault(String key, Object value) {
        if(! map.containsKey(key)) {
            map.put(key, value);
        }
    }

    public Object remove(String key) {
        return map.remove(key);
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public Map<String, Object> toMap() {
        return new HashMap<>(map);
    }
}
