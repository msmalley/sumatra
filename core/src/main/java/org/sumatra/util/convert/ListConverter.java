package org.sumatra.util.convert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sumatra.util.ReflectionUtil;

public class ListConverter extends BaseConverter {
   public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType) {
       if(obj instanceof List) {
           List list = (List) obj;

           if(destType.isArray()) {
               Class<?> elemType = destType.getComponentType();
               Object[] dest = ReflectionUtil.createArray(elemType, list.size());
               copyToArray(ctx, list, dest);
               return new ConversionResult(dest);
           }

           Collection<Object> dest = ReflectionUtil.createCollection(destType, ArrayList.class);

           if(dest != null) {
               copy(ctx, list, dest);
               return new ConversionResult(dest);
           }
       }

       return new ConversionResult<>();
    }
}
