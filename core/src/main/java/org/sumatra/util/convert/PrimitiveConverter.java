package org.sumatra.util.convert;

import java.util.Arrays;
import java.util.List;

public class PrimitiveConverter implements ValueConverter {
    private static final List<Class<?>> WRAPPER_CLASSES = Arrays.asList(
            Boolean.class,
            Byte.class,
            Character.class,
            Short.class,
            Integer.class,
            Long.class,
            Float.class,
            Double.class,
            Void.class,
            String.class);

    @Override
    public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType) {
        if(destType.isPrimitive()) {
            if(boolean.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)Boolean.FALSE);
                }

                if(obj instanceof Boolean) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(byte.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)new Byte((byte)0));
                }

                if(obj instanceof Number) {
                    return new ConversionResult<>((T)Byte.valueOf(((Number)obj).byteValue()));
                }

                if(obj instanceof Byte) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(char.class.equals(destType)) {
                if(obj instanceof Character) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(short.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)new Short((short)0));
                }

                if(obj instanceof Number) {
                    return new ConversionResult<>((T)Short.valueOf(((Number)obj).shortValue()));
                }

                if(obj instanceof Short) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(int.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)new Integer(0));
                }

                if(obj instanceof Number) {
                    return new ConversionResult<>((T)Integer.valueOf(((Number)obj).intValue()));
                }

                if(obj instanceof Integer) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(long.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)new Long(0L));
                }

                if(obj instanceof Number) {
                    return new ConversionResult<>((T)Long.valueOf(((Number)obj).longValue()));
                }

                if(obj instanceof Long) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(float.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)new Float(0f));
                }

                if(obj instanceof Number) {
                    return new ConversionResult<>((T)Float.valueOf(((Number)obj).floatValue()));
                }

                if(obj instanceof Float) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(double.class.equals(destType)) {
                if(obj == null) {
                    return new ConversionResult<>((T)new Double(0.0));
                }

                if(obj instanceof Number) {
                    return new ConversionResult<>((T)Double.valueOf(((Number)obj).doubleValue()));
                }

                if(obj instanceof Double) {
                    return new ConversionResult<>((T)obj);
                }
            } else if(void.class.equals(destType)) {
                if(obj instanceof Void) {
                    return new ConversionResult<>((T)obj);
                }
            }
        }

        if(obj == null) {
            return new ConversionResult<>(true, null);
        }

        if(WRAPPER_CLASSES.contains(obj.getClass())) {
            if(destType.isInstance(obj)) {
                return new ConversionResult<>((T)obj);
            }
        }

        return new ConversionResult<>();
    }
}
