package org.sumatra.util.convert;

public class ConversionResult<T> {
    public boolean success;
    public T result;

    public ConversionResult() {
        this(false, null);
    }

    public ConversionResult(T result) {
        this(true, result);
    }

    public ConversionResult(boolean success, T result) {
        this.success = success;
        this.result = result;
    }
}
