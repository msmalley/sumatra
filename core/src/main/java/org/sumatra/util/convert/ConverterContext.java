package org.sumatra.util.convert;

public class ConverterContext {
    public Converter converter;

    public ConverterContext(Converter converter) {
        this.converter = converter;
    }

    public Converter getConverter() {
        return converter;
    }
}
