package org.sumatra.util.convert;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.sumatra.util.ReflectionUtil;

public class SetConverter extends BaseConverter {
    public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType) {
        if(obj instanceof Set) {
            Set set = (Set) obj;

            if(destType.isArray()) {
                Class<?> elemType = destType.getComponentType();
                Object[] dest = ReflectionUtil.createArray(elemType, set.size());
                copyToArray(ctx, set, dest);
                return new ConversionResult(dest);
            }

            Collection<Object> dest = ReflectionUtil.createCollection(destType, HashSet.class);

            if(dest != null) {
                copy(ctx, set, dest);
                return new ConversionResult(dest);
            }
        }

        return new ConversionResult<>();
    }
}
