package org.sumatra.util.convert;

import java.util.ArrayList;
import java.util.List;

public class Converter {
    public static Converter standardConverter() {
        Converter oc = new Converter();

        oc.addConverter(new PrimitiveConverter());
        oc.addConverter(new DateConverter());
        oc.addConverter(oc.beanConverter); // needs to come before MapConverter
        oc.addConverter(new MapConverter());
        oc.addConverter(new ListConverter());
        oc.addConverter(new SetConverter());
        oc.addConverter(new ArrayConverter());

        return oc;
    }

    protected List<ValueConverter> converters = new ArrayList<>();
    protected BeanConverter beanConverter = new BeanConverter();

    public Converter() {}

    public BeanConverter getBeanConverter() {
        return beanConverter;
    }

    public void addConverter(ValueConverter converter) {
        this.converters.add(converter);
    }

    public <T> T convert(Object obj, Class<T> destType) {
        return convert(new ConverterContext(this), obj, destType);
    }

    public <T> T convert(ConverterContext ctx, Object obj, Class<T> destType) {
        for(ValueConverter converter : converters) {
            ConversionResult<T> result = converter.convert(ctx, obj, destType);

            if(result.success) {
                return result.result;
            }
        }

        throw new ConverterException("can't convert " + obj + " to " + destType);
    }
}
