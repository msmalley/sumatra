package org.sumatra.util.convert;

import java.util.Collection;

public abstract class BaseConverter implements ValueConverter {
    protected static void copy(ConverterContext ctx, Collection<Object> src, Collection<Object> dest) {
        for(Object item : src) {
            Object convertedItem = ctx.getConverter().convert(ctx, item, Object.class);
            dest.add(convertedItem);
        }
    }

    protected static void copyFromArray(ConverterContext ctx, Object[] src, Collection<Object> dest) {
        for(int i = 0; i < src.length; i++) {
            Object elem = ctx.getConverter().convert(ctx, src[i], Object.class);
            dest.add(elem);
        }
    }

    protected static void copyToArray(ConverterContext ctx, Collection<Object> src, Object[] dest) {
        int i = 0;

        for(Object elem : src) {
            elem = ctx.getConverter().convert(ctx, elem, Object.class);
            dest[i++] = elem;
        }
    }
}
