package org.sumatra.util.convert;

import java.util.HashMap;
import java.util.Map;

import org.sumatra.util.ReflectionUtil;
import org.sumatra.util.beans.BeanDescriptor;
import org.sumatra.util.beans.BeanManager;
import org.sumatra.util.beans.PropertyDescriptor;
import org.sumatra.util.beans.TypeDescriptor;

public class BeanConverter implements ValueConverter {
    protected BeanManager manager = new BeanManager();
    protected Map<String, TypeDescriptor> types = new HashMap<>();

    public BeanConverter() {}

    public void registerType(Class<?> type) {
        TypeDescriptor typeInfo = new TypeDescriptor(type);
        types.put(typeInfo.typeId, typeInfo);
    }

    public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType) {
        if(obj instanceof Map) {
            // converting from serialized value to bean instance

            Map<Object,Object> map = (Map<Object,Object>)obj;

            for(TypeDescriptor typeInfo : types.values()) {
                // first check is to ensure the current type being tried
                // is compatible with the request destType

                if(! destType.isAssignableFrom(typeInfo.type)) {
                    continue;
                }

                // next check is that the typeId is actually a string.
                // this is probably overly cautious but it's easy to check
                Object typeInfoObj = map.get(typeInfo.key);

                if(! (typeInfoObj instanceof String)) {
                    continue;
                }

                String actualTypeId = (String)typeInfoObj;
                Class<?> actualType = typeInfo.getActualType(actualTypeId);

                if(actualType != null) {
                    return new ConversionResult<>((T) fromMap(ctx, map, actualType));
                }
            }
        } else if(Map.class.equals(destType) || Object.class.equals(destType)) {
            for(TypeDescriptor typeInfo : types.values()) {
                if(typeInfo.type.isInstance(obj)) {
                    // converting bean instance to serialized value (map)
                    return new ConversionResult<>((T) toMap(ctx, obj, typeInfo));
                }
            }
        }

        return new ConversionResult<>();
    }

    protected Map<String,Object> toMap(ConverterContext ctx, Object bean, TypeDescriptor typeInfo) {
        if(bean == null) {
            return null;
        }

        BeanDescriptor beanInfo = manager.getBeanInfo(bean.getClass());
        Map<String,PropertyDescriptor> properties = beanInfo.getProperties();
        Map<String,Object> map = new HashMap<>();

        map.put(typeInfo.key, typeInfo.getActualTypeId(beanInfo));

        for(PropertyDescriptor property : properties.values()) {
            if(! property.isReadable()) {
                continue;
            }

            Object value = property.get(bean, ctx);

            if(value != null) {
                value = ctx.getConverter().convert(ctx, value, Object.class);
            }

            map.put(property.getName(), value);
        }

        return map;
    }

    protected <T> T fromMap(ConverterContext ctx, Map<?,?> map, Class<T> cls) {
        BeanDescriptor beanInfo = manager.getBeanInfo(cls);
        Map<String,PropertyDescriptor> properties = beanInfo.getProperties();
        T bean = ReflectionUtil.instantiate(cls);

        for(PropertyDescriptor property : properties.values()) {
            if(! property.isWritable()) {
                continue;
            }

            Object value = map.get(property.getName());
            value = ctx.getConverter().convert(ctx, value, property.getType());

            property.set(bean, value, ctx);
        }

        return bean;
    }

}
