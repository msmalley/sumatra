package org.sumatra.util.convert;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sumatra.util.ReflectionUtil;

public class MapConverter implements ValueConverter {
    protected Class<?> defaultMapType = HashMap.class;

    public <T> ConversionResult convert(ConverterContext ctx, Object obj, Class<T> destType) {
        if(obj instanceof Map) {
            Map<Object,Object> map = (Map<Object,Object>)obj;
            Map<Object,Object> dest = null;

            if(destType.equals(Object.class) || destType.equals(Map.class)) {
                dest = ReflectionUtil.createMap((Class<Map<Object, Object>>) defaultMapType);
            } else if(! Modifier.isAbstract(destType.getModifiers())) {
                if(Map.class.isAssignableFrom(destType)) {
                    dest = ReflectionUtil.createMap((Class<Map<Object, Object>>) destType);
                }
            }

            if(dest != null) {
                for(Object key : map.keySet()) {
                    dest.put(key, ctx.getConverter().convert(ctx, map.get(key), Object.class));
                }

                return new ConversionResult<>((T)dest);
            }
        }

        return new ConversionResult<>();
    }
}
