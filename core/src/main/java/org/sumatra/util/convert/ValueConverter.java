package org.sumatra.util.convert;

public interface ValueConverter {
    public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType);
}
