package org.sumatra.util.convert;

import java.util.List;
import java.util.Set;

import org.sumatra.util.ReflectionUtil;

public class ArrayConverter extends BaseConverter {
    @Override
    public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType) {
        if(obj.getClass().isArray()) {
            Object[] array = (Object[])obj;

            if(destType.isArray()) {
                Class<?> destElemType = destType.getComponentType();
                Object[] dest = ReflectionUtil.createArray(destElemType, array.length);

                for(int i = 0; i < array.length; i++) {
                    Object elem = array[i];
                    elem = ctx.getConverter().convert(ctx, elem, destElemType);
                    dest[i] = elem;
                }

                return new ConversionResult<>((T)dest);
            }

            if(List.class.isAssignableFrom(destType)) {
                List<Object> dest = ReflectionUtil.createList((Class<List>)destType);
                copyFromArray(ctx, array, dest);
                return new ConversionResult<>((T)dest);
            }

            if(Set.class.isAssignableFrom(destType)) {
                Set<Object> dest = ReflectionUtil.createSet((Class<Set>)destType);
                copyFromArray(ctx, array, dest);
                return new ConversionResult<>((T)dest);
            }
        }

        return new ConversionResult<>();
    }
}
