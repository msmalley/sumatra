package org.sumatra.util.convert;

import java.util.Date;

import org.sumatra.util.DateUtil;

public class DateConverter implements ValueConverter {
    public <T> ConversionResult<T> convert(ConverterContext ctx, Object obj, Class<T> destType) {
        if(obj instanceof Date && destType.isAssignableFrom(String.class)) {
            return new ConversionResult<>((T) DateUtil.format((Date) obj));
        }

        if(obj instanceof String) {
            String dateStr = (String)obj;
            Date date = DateUtil.parse(dateStr);

            if(date != null) {
                if(destType.isAssignableFrom(Date.class)) {
                    return new ConversionResult<>((T)date);
                }

                if(destType.isAssignableFrom(Long.class)) {
                    return new ConversionResult<>((T) Long.valueOf(date.getTime()));
                }
            }
        }

        return new ConversionResult<>();
    }
}
