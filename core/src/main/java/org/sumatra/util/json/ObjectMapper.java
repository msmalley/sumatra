package org.sumatra.util.json;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sumatra.util.ReflectionUtil;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class ObjectMapper {
    protected Map<String,Class<?>> types = new HashMap<>();
    protected JsonFactory jsonFactory = new JsonFactory();
    protected DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
    protected String typeIdKey = "_type";

    public void registerType(Class<Object> cls) {
        registerType(cls.getName(), cls);
    }

    public void registerType(String name, Class<Object> cls) {
        types.put(name, cls);
    }

    public Object readObject(String string) {
        return readObject(string, Object.class);
    }

    public <T> T readObject(String string, Class<T> expectedType) {
        try {
            JsonParser parser = jsonFactory.createParser(string);
            T object = (T)parseObject(parser);
            parser.close();
            return object;
        } catch (IOException e) {
            throw new MappingException(e);
        }
    }

    public Object readObject(InputStream stream) {
        return readObject(stream, Object.class);
    }

    public <T> T readObject(InputStream stream, Class<T> expectedType) {
        try {
            JsonParser parser = jsonFactory.createParser(stream);
            T object = (T)parseObject(parser);
            parser.close();
            return object;
        } catch (IOException e) {
            throw new MappingException(e);
        }
    }

    public Object readObject(Reader reader) {
        return readObject(reader, Object.class);
    }

    public <T> T readObject(Reader reader, Class<T> expectedType) {
        try {
            JsonParser parser = jsonFactory.createParser(reader);
            T object = (T)parseObject(parser);
            parser.close();
            return object;
        } catch (IOException e) {
            throw new MappingException(e);
        }
    }

    public Object parse(JsonParser parser) throws IOException {
        JsonToken token = parser.nextToken();
        Object result = null;

        if(token == JsonToken.START_OBJECT) {
            return parseObject(parser);
        } else if(token == JsonToken.START_ARRAY) {
            return parseArray(parser);
        } else if(token == JsonToken.VALUE_STRING) {
            return parser.getValueAsString();
        } else if(token == JsonToken.VALUE_TRUE || token == JsonToken.VALUE_FALSE) {
            return parser.getValueAsBoolean();
        } else if(token == JsonToken.VALUE_NUMBER_INT) {
            return parser.getValueAsLong();
        } else if(token == JsonToken.VALUE_NUMBER_FLOAT) {
            return parser.getValueAsDouble();
        } else if(token == JsonToken.VALUE_NULL) {
            return null;
        }

        throw new IllegalStateException("unsupported token " + token + " @ " + parser.getTokenLocation());
    }

    public Object parseObject(JsonParser parser) throws IOException {
        Map<String,Object> map = new HashMap<>();

        while(parser.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = parser.getCurrentName();
            Object value = parse(parser);

            map.put(fieldName, value);
        }

        if(map.containsKey(typeIdKey)) {
            String typeId = (String)map.get(typeIdKey);
            Class<?> type = locateType(typeId);
            Object instance = ReflectionUtil.instantiate(type);
            apply(map, instance);
            return instance;
        }

        return map;
    }

    public Object parseArray(JsonParser parser) throws IOException {
        List<Object> list = new ArrayList<>();

        while(parser.nextToken() != JsonToken.END_ARRAY) {
            list.add(parse(parser));
        }

        return list;
    }

    public Class<?> locateType(String typeId) {
        Class<?> potentialType;

        if(types.containsKey(typeId)) {
            potentialType = types.get(typeId);
        } else {
            try {
                potentialType = Class.forName(typeId);
            } catch (ClassNotFoundException e) {
                throw new MappingException(e);
            }
        }

        return potentialType;
    }

    public void apply(Map<String,Object> map, Object bean) {
        for(Map.Entry<String,Object> entry : map.entrySet()) {
            try {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(entry.getKey(), bean.getClass());
                Class<?> expectedType = propertyDescriptor.getPropertyType();
                Object value = convert(entry.getValue(), expectedType);
                propertyDescriptor.setValue(entry.getKey(), bean);
            } catch (Exception e) {
                throw new MappingException(e);
            }
        }
    }

    public <T> T convert(Object value, Class<T> destType) {
        if(value == null) {
            return null;
        }

        if(value instanceof String) {
            if(Date.class.equals(destType)) {
                try {
                    return (T)dateFormat.parse((String)value);
                } catch(ParseException e) {
                    throw new MappingException(e);
                }
            }
            return (T)value;
        }

        if(value instanceof List) {
            List<?> list = (List<?>)value;

            if(List.class.isAssignableFrom(destType)) {
                List<Object> converted = new ArrayList<>();

                for(Object item : list) {
                    converted.add(convert(item, Object.class));
                }
            } else if(Set.class.isAssignableFrom(destType)) {
                Set<Object> converted = new HashSet<>();

                for(Object item : list) {
                    converted.add(convert(item, Object.class));
                }

                return (T)converted;
            } else if(destType.isArray()) {
                Object[] converted = (Object[]) Array.newInstance(destType, list.size());

                for(int i = 0; i < list.size(); i++) {
                    converted[i] = convert(list.get(i), Object.class);
                }

                return (T)converted;
            }
        }

        if(value instanceof Map) {
            Map<String,Object> map = (Map<String,Object>)value;
            Map<String,Object> converted = new HashMap<>();

            for(Map.Entry<String,Object> entry : map.entrySet()) {
                converted.put(entry.getKey(), convert(entry.getValue(), Object.class));
            }

            return (T)converted;
        }

        if(destType.isInstance(value)) {
            return (T)value;
        }

        throw new MappingException("Don't know how to convert " + value + " to type " + destType);
    }

    public static class MappingException extends RuntimeException {
        public MappingException(String message) {
            super(message);
        }

        public MappingException(Exception e) {
            super(e);
        }
    }
}
