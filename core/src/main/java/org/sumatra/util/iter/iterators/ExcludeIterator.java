package org.sumatra.util.iter.iterators;

import java.util.Iterator;

import org.sumatra.util.iter.IterFilter;

public class ExcludeIterator<T> extends FilteredIterator<T> {
	public ExcludeIterator(Iterator<T> iterator, IterFilter<T> filter) {
		super(iterator, filter);
	}
	
	@Override
	public boolean test(T obj) {
		return ! this.filter.test(obj);
	}
}
