package org.sumatra.util.iter.iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PeekableIterator<T> implements Iterator<T> {
	protected Iterator<T> iterator;
	protected T nextObj;
	protected boolean initialized = false;
	
	public PeekableIterator(Iterator<T> iterator) {
		this.iterator = iterator;
	}
	
	protected PeekableIterator() {}
	
	public boolean isInitialized() {
		return this.initialized;
	}
	
	protected void init() {
		if(! this.initialized) {			
			this.nextObj = this._next();
			this.initialized = true;
		}
	}
	
	protected void checkHasNext() {
		this.init();
		if(this.nextObj == null) {
			throw new NoSuchElementException();
		}
	}
	
	public T peek() {
		this.checkHasNext();
		return this.nextObj;
	}
	
	@Override
	public boolean hasNext() {
		this.init();
		return this.nextObj != null;
	}

	protected T _next() {
		if(this.iterator.hasNext()) {
			return this.iterator.next();
		} else {
			return null;
		}
	}
	
	@Override
	public T next() {
		this.init();
		this.checkHasNext();
		T next = this.nextObj;
		this.nextObj = this._next();
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
