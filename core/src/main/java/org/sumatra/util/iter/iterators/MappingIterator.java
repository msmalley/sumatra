package org.sumatra.util.iter.iterators;

import java.util.Iterator;

import org.sumatra.util.iter.IterMapper;

public class MappingIterator<X,Y> extends PeekableIterator<Y> {
	protected Iterator<X> iterator;
	protected IterMapper<X,Y> mapper;
	
	public MappingIterator(Iterator<X> iterator, IterMapper<X,Y> mapper) {
		this.iterator = iterator;
		this.mapper = mapper;
	}
	
	@Override
	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	@Override
	public Y next() {
		try {
			return this.mapper.map(this.iterator.next());
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void remove() {
		this.iterator.remove();
	}
}
