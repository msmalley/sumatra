package org.sumatra.util.iter.iterators;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {
	protected int index = 0;
	protected T[] array;
	
	public ArrayIterator(T...elems) {
		this.array = elems;
	}
	
	@Override
	public boolean hasNext() {
		return this.index < this.array.length;
	}

	@Override
	public T next() {
		return this.array[this.index++];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
