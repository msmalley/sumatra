package org.sumatra.util.iter.iterators;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class UnwindingIterator<T> implements Iterator<T> {
	protected Iterator<List<T>> iterator;
	protected List<T> currentList;
	protected int index = 0;
	
	public UnwindingIterator(Iterator<List<T>> iterator) {
		this.iterator = iterator;
	}
	
	@Override
	public boolean hasNext() {
		return currentList != null || iterator.hasNext();
	}

	@Override
	public T next() {
		if(currentList == null) {
			if(! iterator.hasNext()) {
				throw new NoSuchElementException();
			}
			
			currentList = iterator.next();
			index = 0;
		}
		
		T next = currentList.get(index++);
		
		if(index == currentList.size()) {
			currentList = null;
		}
		
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
