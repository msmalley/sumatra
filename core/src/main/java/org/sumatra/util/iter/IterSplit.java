package org.sumatra.util.iter;

public class IterSplit<T> {
	public Iter<T> left;
	public Iter<T> right;
	
	public IterSplit(Iter<T> left, Iter<T> right) {
		this.left = left;
		this.right = right;
	}
}

