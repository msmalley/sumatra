package org.sumatra.util.iter;

import java.util.Iterator;
import java.util.List;

public class ListIter<T> extends Iter<List<T>> {
	public ListIter(Iterator<List<T>> iterator) {
		super(iterator);
	}
	
	public Iter<T> unwind() {
		return IterTools.unwind(iterator());
	} 
}
