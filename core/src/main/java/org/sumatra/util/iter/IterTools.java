package org.sumatra.util.iter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.sumatra.util.iter.iterators.CharIterator;
import org.sumatra.util.iter.iterators.RangeIterator;
import org.sumatra.util.iter.iterators.UnwindingIterator;

public class IterTools {
	public static <T> Iter<T> iter(final T elem) {
		return iter(list(elem));
	}
	
	public static <T> Iter<T> iter(T...elems) {
		return new Iter<T>(elems);
	}
	
	public static <T> Iter<T> iter(Collection<T> coll) {
		return new Iter<T>(coll);
	}
	
	public static <T> Iter<T> iter(Iterator<T> iterator) {
		return new Iter<T>(iterator);
	}
	
	public static Iter<Character> iter(char[] chars) {
		return iter(new CharIterator(chars));
	}
	
	public static Iter<Integer> range() {
		return range(null);
	}
	
	public static Iter<Integer> range(Integer end) {
		return range(0, end, 1);
	}
	
	public static Iter<Integer> range(Integer start, Integer end) {
		return range(start, end, 1);
	}
	
	public static Iter<Integer> range(final Integer start, final Integer end, final Integer inc) {
		return iter(new RangeIterator(start, end, inc));
	}
		
	public static <T> Set<T> set(Iterable<T> iterable) {
		Set<T> set = new HashSet<T>();
		
		for(T obj : iterable) {
			set.add(obj);
		}
		
		return set;
	}
	
	public static <T> Set<T> set(T...elems) {
		return set(iter(elems));
	}
	
	public static <T> List<T> list(Iterable<T> iterable) {
		List<T> list = new ArrayList<T>();
		
		for(T obj : iterable) {
			list.add(obj);
		}
		
		return list;
	}
	
	public static <T> List<T> list(T elem) {
		List<T> list = new ArrayList<T>();
		list.add(elem);
		return list;
	}
	
	public static <T> List<T> list(T...elems) {
		return Arrays.asList(elems);
	}
	
	public static String join(final String delim, Iter<String> iter) {
		return iter.reduce(new IterReducer<String>() {
			@Override
			public String reduce(String left, String right) {
				return left + delim + right;
			}
		});
	}
	
	public static String join(String delim, String...strs) {
		return join(delim, iter(strs));
	}
	
	public static String concat(Iter<String> iter) {
		return join("", iter);
	}
	
	public static String concat(String...strs) {
		return concat(iter(strs));
	}
	
	public static <T> Iter<T> unwind(Iterator<List<T>> iterator) {
		return new Iter<T>(new UnwindingIterator<T>(iterator));
	}
}
