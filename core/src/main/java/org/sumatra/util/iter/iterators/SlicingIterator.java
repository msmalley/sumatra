package org.sumatra.util.iter.iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SlicingIterator<T> implements Iterator<T> {
	protected Iterator<T> iterator;
	protected Integer start, end, inc, current;
	
	/**
	 * 
	 * @param iterator
	 * @param start Inclusive start index
	 * @param end Exclusive end index
	 * @param inc
	 */
	public SlicingIterator(Iterator<T> iterator, Integer start, Integer end, Integer inc) {
		if(start == null) {
			start = 0;
		}
		
		if(inc == null) {
			inc = 1;
		}
		
		if(start < 0 || (end != null && end < 0)) {
			throw new IllegalArgumentException("start and end must be greater than or equal to zero");
		} else if(end != null && start > end) {
			throw new IllegalArgumentException("start greater than end");
		} else if(inc <= 0) {
			throw new IllegalArgumentException("inc must be greater than zero");
		}

		this.iterator = iterator;
		this.start = start;
		this.end = end;
		this.inc = inc;
		this.current = 0;
	}

	@Override
	public boolean hasNext() {
		return this.iterator.hasNext() && (this.end == null || (this.current + this.inc) <= this.end);
	}
	
	@Override
	public T next() {
		int stop = this.current <= this.start ? this.start + 1 : this.current + this.inc;
		T next = null;
		
		while(this.iterator.hasNext() && this.current < stop) {
			next = this.iterator.next();
			this.current++;
		}
		
		if(next == null) {
			throw new NoSuchElementException();
		}
		
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
