package org.sumatra.util.iter;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.sumatra.util.iter.iterators.ArrayIterator;
import org.sumatra.util.iter.iterators.ChainingIterator;
import org.sumatra.util.iter.iterators.ExcludeIterator;
import org.sumatra.util.iter.iterators.FilteredIterator;
import org.sumatra.util.iter.iterators.MappingIterator;
import org.sumatra.util.iter.iterators.PeekableIterator;
import org.sumatra.util.iter.iterators.SlicingIterator;

public class Iter<T> implements Iterable<T>, Iterator<T> {
	protected PeekableIterator<T> iterator;
	
	public Iter(T...objs) {
		this(new ArrayIterator<T>(objs));
	}
	
	public Iter(Collection<T> coll) {
		this(coll.iterator());
	}
	
	public Iter(Iterator<T> iterator) {
		if(iterator instanceof PeekableIterator) {
			this.iterator = (PeekableIterator<T>)iterator;
		} else {
			this.iterator = new PeekableIterator<T>(iterator);
		}
	}
	
	public boolean isInitialized() {
		return this.iterator.isInitialized();
	}
	
	public T head() {
		return this.iterator.peek();
	}
	
	public Iter<T> tail() {
		if(this.hasNext()) {
			this.next();
		}
		return this;
	}
	
	public Iter<T> filter(IterFilter<T> filter) {
		return IterTools.iter(new FilteredIterator<T>(this.iterator, filter));
	}
	
	public Iter<T> exclude(IterFilter<T> filter) {
		return IterTools.iter(new ExcludeIterator<T>(this.iterator, filter));
	}
	
	public <T2> Iter<T2> map(IterMapper<T,T2> mapper) {
		return new Iter<T2>(new MappingIterator<T,T2>(this.iterator(), mapper));
	}
	
	public T reduce(IterReducer<T> reducer) {
		return reduce(null, reducer);
	}
	
	public T reduce(T start, IterReducer<T> reducer) {
		T reduced;
		
		if(start == null) {
			start = this.next();
		}
		
		reduced = start;
		
		for(T obj : this) {
			reduced = reducer.reduce(reduced, obj);
		}
		
		return reduced;
	}
	
	@SuppressWarnings("unchecked")
	public <E> ListIter<E> listIter(Class<E> cls) {
		return new ListIter<E>((Iterator<List<E>>) this.iterator());
	}
	
	public List<T> list() {
		return IterTools.list(this);
	}
	
	public Set<T> set() {
		return IterTools.set(this);
	}
	
	@SuppressWarnings("unchecked")
	public T[] array() {
		List<T> list = this.list();
		return (T[])list.toArray(new Object[list.size()]);
	}
	
	public Iter<T> chain(T elem) {
		return this.chain(IterTools.iter(elem));
	}
	
	public Iter<T> chain(Iter<T> other) {
		return IterTools.iter(new ChainingIterator<T>(this.iterator(), other.iterator()));
	}
	
	public Iter<T> slice(Integer end) {
		return this.slice(0, end, 1);
	}
	
	public Iter<T> slice(Integer start, Integer end) {
		return this.slice(start, end, 1);
	}
	
	public Iter<T> slice(Integer start, Integer end, Integer inc) {
		return IterTools.iter(new SlicingIterator<T>(this.iterator(), start, end, inc));
	}
	
	public Iter<T> sort() {
		return this.sort(null);
	}
	
	public Iter<T> sort(Comparator<T> comparator) {
		List<T> list = this.list();
		Collections.sort(list, comparator);
		return new Iter<T>(list.iterator());
	}
	
	public Iter<T> reverse() {
		if(! this.hasNext()) {
			return this;
		}
		T head = this.head();
		return this.tail().reverse().chain(head);
	}
	
	/**
	 * Split the iter into two (left and right) at the given index. This will advance the iter
	 * up to the given index and hold those object in memory.
	 * @param index
	 * @return
	 */
	public IterSplit<T> split(Integer index) {
		Iter<T> left = IterTools.iter(this.slice(index).list());
		return new IterSplit<T>(left, this);
	}
	
	/*
	 * Iterable
	 */
	
	@Override
	public PeekableIterator<T> iterator() {
		return this.iterator;
	}
	
	/*
	 * Iterator
	 */
	
	@Override
	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	@Override
	public T next() {
		return this.iterator.next();
	}

	@Override
	public void remove() {
		this.iterator.remove();
	}
}
