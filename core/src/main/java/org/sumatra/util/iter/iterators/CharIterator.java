package org.sumatra.util.iter.iterators;

import java.util.Iterator;

public class CharIterator implements Iterator<Character> {
	protected int index = 0;
	protected char[] chars;
	
	public CharIterator(char[] chars) {
		this.chars = chars;
	}
	
	@Override
	public boolean hasNext() {
		return this.index < chars.length;
	}

	@Override
	public Character next() {
		return this.chars[this.index++];
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
