package org.sumatra.util.iter.iterators;

import java.util.Iterator;


public class ChainingIterator<T> implements Iterator<T> {
	protected Iterator<T> left, right, current;
	
	public ChainingIterator(Iterator<T> left, Iterator<T> right) {
		this.left = left;
		this.right = right;
		this.current = this.left;
	}
	
	protected void updateCurrent() {
		if(this.current == this.left && ! this.current.hasNext()) {
			this.current = this.right;
		}
	}
	
	@Override
	public boolean hasNext() {
		this.updateCurrent();
		return this.current.hasNext();
	}

	@Override
	public T next() {
		this.updateCurrent();
		return this.current.next();
	}

	@Override
	public void remove() {
		this.updateCurrent();
		this.current.remove();
	}
}
