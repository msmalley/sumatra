package org.sumatra.util.iter.iterators;

import java.util.Iterator;

import org.sumatra.util.iter.IterFilter;

public class FilteredIterator<T> extends PeekableIterator<T> {
	protected IterFilter<T> filter;
	
	public FilteredIterator(Iterator<T> iterator, IterFilter<T> filter) {
		super(iterator);
		this.filter = filter;
	}
	
	public boolean test(T obj) {
		return this.filter.test(obj);
	}
	
	@Override
	protected T _next() {
		this.nextObj = null;
		
		while(this.iterator.hasNext()) {
			T next = this.iterator.next();
			if(this.test(next)) {
				this.nextObj = next;
				break;
			}
		}
		
		return this.nextObj;
	}
}
