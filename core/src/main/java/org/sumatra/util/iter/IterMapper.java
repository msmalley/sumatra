package org.sumatra.util.iter;

public interface IterMapper<X,Y> {
	public Y map(X obj);
}