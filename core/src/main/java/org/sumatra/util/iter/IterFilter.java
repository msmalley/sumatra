package org.sumatra.util.iter;

public interface IterFilter<T> {
	public boolean test(T obj);
}
