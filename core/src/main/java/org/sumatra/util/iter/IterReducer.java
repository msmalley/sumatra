package org.sumatra.util.iter;

public interface IterReducer<T> {
	public T reduce(T left, T right);
}
