package org.sumatra.util.iter.iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RangeIterator implements Iterator<Integer> {
	protected Integer index, start, end, inc;
	
	public RangeIterator(Integer start, Integer end, Integer inc) {
		if(inc > 0 && start > end) {
			throw new IllegalArgumentException("start greater than end");
		} else if(inc < 0 && end > start) {
			throw new IllegalArgumentException("end greater than start");
		}
		
		if(inc == 0) {
			this.index = end;
		} else {
			this.index = start;
		}
		
		this.start = start;
		this.end = end;
		this.inc = inc;
	}

	@Override
	public boolean hasNext() {
		return this.end == null || this.index < this.end;
	}

	@Override
	public Integer next() {
		if(this.index == this.end) {
			throw new NoSuchElementException();
		}
		Integer next = this.index;
		this.index += this.inc;
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
