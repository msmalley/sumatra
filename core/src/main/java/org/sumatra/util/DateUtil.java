package org.sumatra.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateUtil {
    public static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    public static final DateFormat ISO8601_DATE = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat ISO8601_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static final DateFormat ISO8601_DATE_TIME_TZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final DateFormat ISO8601_DATE_TIME_MS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    public static final DateFormat ISO8601_DATE_TIME_MS_TZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private static List<DateFormat> formats = new ArrayList<>();

    static {
        ISO8601_DATE_TIME.setTimeZone(GMT);
        ISO8601_DATE_TIME_MS.setTimeZone(GMT);

        formats.add(ISO8601_DATE);
        formats.add(ISO8601_DATE_TIME);
        formats.add(ISO8601_DATE_TIME_TZ);
        formats.add(ISO8601_DATE_TIME_MS);
    }

    public static String format(Date date) {
        return ISO8601_DATE_TIME_MS_TZ.format(date);
    }

    public static Date parse(String dateStr) {
        for(DateFormat format : formats) {
            try {
                return format.parse(dateStr);
            } catch (ParseException e) {
                continue;
            }
        }

        return null;
    }
}
