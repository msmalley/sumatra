package org.sumatra.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class JsonUtil {
    protected static final ObjectMapper objectMapper = new ObjectMapper();

    public static class ObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {
        public ObjectMapper() {
            super();

            // see http://wiki.fasterxml.com/JacksonFAQDateHandling
            configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            getSerializationConfig().with(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));

            SimpleModule module = new SimpleModule("SumatraModule", new Version(1, 0, 0, null, "org.brightstatic.sumatra", "core"));
            module.addSerializer(Dictionary.class, new DictionarySerializer());
            module.addDeserializer(Dictionary.class, new DictionaryDeserializer());

            registerModule(module);
        }
    }

    public static class DictionarySerializer extends JsonSerializer<Dictionary> {
        @Override
        public void serialize(Dictionary value, JsonGenerator generator, SerializerProvider provider) throws IOException {
            provider.defaultSerializeValue(value.map, generator);
        }
    }

    public static class DictionaryDeserializer extends JsonDeserializer<Dictionary> {
        @Override
        public Dictionary deserialize(JsonParser parser, DeserializationContext context) throws IOException {
            Map<String,Object> map = parser.readValueAs(new TypeReference<Map<String,Object>>() {});
            return new Dictionary(map);
        }
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }
}
