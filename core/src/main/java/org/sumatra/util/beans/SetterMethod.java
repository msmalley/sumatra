package org.sumatra.util.beans;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SetterMethod implements SetterDescriptor {
    protected Method method;

    public SetterMethod(Method method) {
        if(method.getParameterTypes().length == 0) {
            throw new IllegalArgumentException("invalid signature for bean setter method: " + method);
        }

        this.method = method;
    }

    @Override
    public Class<?> getType() {
        return method.getReturnType();
    }

    @Override
    public void set(Object bean, Object value, Object... extraArgs) {
        int numArgs = method.getParameterTypes().length;
        Object[] args = new Object[numArgs];
        int numExtraArgs = extraArgs.length;

        if(numExtraArgs > numArgs - 1) {
            numExtraArgs = numArgs - 1;
        }

        args[0] = value;
        System.arraycopy(extraArgs, 0, args, 1, numExtraArgs);

        try {
            method.invoke(bean, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new BeanException(e);
        }
    }
}
