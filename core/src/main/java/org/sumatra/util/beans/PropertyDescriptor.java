package org.sumatra.util.beans;

public class PropertyDescriptor {
    protected String name;
    protected GetterDescriptor getter;
    protected SetterDescriptor setter;

    public PropertyDescriptor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Class<?> getType() {
        if(getter != null) {
            return getter.getType();
        }

        if(setter != null) {
            return setter.getType();
        }

        return Object.class;
    }

    public boolean isReadable() {
        return getter != null;
    }

    public boolean isWritable() {
        return setter != null;
    }

    public void setGetter(GetterDescriptor getter) {
        this.getter = getter;
    }

    public void setSetter(SetterDescriptor setter) {
        this.setter = setter;
    }

    public Object get(Object bean, Object... extraArgs) {
        if(! isReadable()) {
            throw new BeanException(this + " is not readable");
        }

        return getter.get(bean, extraArgs);
    }

    public void set(Object bean, Object value, Object... extraArgs) {
        if(! isWritable()) {
            throw new BeanException(this+ " is not writable");
        }

        setter.set(bean, value, extraArgs);
    }
}
