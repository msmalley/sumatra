package org.sumatra.util.beans;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface TypeInfo {
    IdFormat idFormat() default IdFormat.fullClassName;
    String key() default "_type";
}
