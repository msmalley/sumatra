package org.sumatra.util.beans;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class BeanDescriptor<T> {
    public static String normalizeName(String name) {
        if(name.startsWith("get") || name.startsWith("set")) {
            return lowerFirst(name.substring(3));
        } else if(name.startsWith("is")) {
            return lowerFirst(name.substring(2));
        }

        return name;
    }

    public static String lowerFirst(String str) {
        if(str.isEmpty()) {
            return str;
        } else if(str.length() == 1) {
            return str.toLowerCase();
        }

        return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    protected Class<T> cls;
    protected Map<String, PropertyDescriptor> properties = new HashMap<>();

    public BeanDescriptor(Class<T> cls) {
        this.cls = cls;

        for(Field field : cls.getFields()) {
            if(field.getAnnotation(Ignore.class) != null) {
                continue;
            }

            String name = field.getName();
            Name customName = field.getAnnotation(Name.class);

            if(customName != null) {
                name = customName.value();
            }

            FieldDescriptor fieldDescriptor = new FieldDescriptor(field);
            PropertyDescriptor property = new PropertyDescriptor(name);
            property.setGetter(fieldDescriptor);
            property.setSetter(fieldDescriptor);
            properties.put(name, property);
        }

        for(Method method : cls.getMethods()) {
            String name = normalizeName(method.getName());
            Name customName = method.getAnnotation(Name.class);

            if(customName != null) {
                name = customName.value();
            }

            boolean isGetter = method.getAnnotation(Getter.class) != null;
            boolean isSetter = method.getAnnotation(Setter.class) != null;

            if(isGetter || isSetter) {
                PropertyDescriptor property = properties.get(name);

                if(property == null) {
                    property = new PropertyDescriptor(name);
                    properties.put(name, property);
                }

                if(isGetter) {
                    property.setGetter(new GetterMethod(method));
                } else if(isSetter) {
                    property.setSetter(new SetterMethod(method));
                }
            }
        }
    }

    public PropertyDescriptor findProperty(String name) {
        return properties.get(name);
    }

    public Map<String, PropertyDescriptor> getProperties() {
        return properties;
    }
}
