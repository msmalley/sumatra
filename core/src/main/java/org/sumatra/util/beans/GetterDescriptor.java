package org.sumatra.util.beans;

public interface GetterDescriptor {
    public Class<?> getType();
    public Object get(Object bean, Object... extraArgs);
}
