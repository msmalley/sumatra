package org.sumatra.util.beans;

public enum IdFormat {
    customName,
    simpleClassName,
    fullClassName
}
