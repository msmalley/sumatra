package org.sumatra.util.beans;

public interface SetterDescriptor {
    public Class<?> getType();
    public void set(Object bean, Object value, Object... extraArgs);
}
