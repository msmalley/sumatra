package org.sumatra.util.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BeanManager {
    protected Map<Class<?>, BeanDescriptor> beanDescriptors = new HashMap<>();

    public BeanDescriptor getBeanInfo(Class<?> cls) {
        BeanDescriptor descriptor = beanDescriptors.get(cls);

        if(descriptor == null) {
            descriptor = new BeanDescriptor(cls);
        }

        return descriptor;
    }

    public Object get(Object bean, String name, Object... extraArgs) {
        Class<?> cls = bean.getClass();
        BeanDescriptor descriptor = getBeanInfo(cls);
        PropertyDescriptor property = descriptor.findProperty(name);

        if(property == null) {
            throw new PropertyNotFoundException(cls + "/" + name + "(" + Arrays.asList(extraArgs) + ")");
        }

        if(property.isReadable()) {
            return property.get(bean, extraArgs);
        }

        throw new BeanException(property + " is not readable");
    }

    public void set(Object bean, String name, Object value, Object... extraArgs) {
        Class<?> cls = bean.getClass();
        BeanDescriptor descriptor = getBeanInfo(cls);
        PropertyDescriptor property = descriptor.findProperty(name);

        if(property == null) {
            throw new PropertyNotFoundException(cls + "/" + name + "(" + Arrays.asList(extraArgs) + ")");
        }

        if(property.isWritable()) {
            property.set(bean, extraArgs);
        }

        throw new BeanException(property + " is not writable");
    }
}
