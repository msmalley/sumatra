package org.sumatra.util.beans;

import org.sumatra.util.ReflectionUtil;

public class TypeDescriptor {
    public String key;
    public IdFormat idFormat;
    public Class<?> type;
    public String typeId;

    @TypeInfo
    private static class DefaultTypeInfo {}

    public TypeDescriptor(Class<?> type) {
        this.type = type;

        TypeInfo typeInfo = ReflectionUtil.findAnnotation(type, TypeInfo.class);

        if(typeInfo == null) {
            typeInfo = DefaultTypeInfo.class.getAnnotation(TypeInfo.class);
        }

        key = typeInfo.key();
        idFormat = typeInfo.idFormat();

        if(idFormat == IdFormat.customName) {
            Name name = type.getAnnotation(Name.class);

            if(name == null) {
                throw new BeanException("@Name missing from " + type);
            }

            typeId = name.value();
        } else if(idFormat == IdFormat.simpleClassName) {
            typeId = type.getSimpleName();
        } else if(idFormat == IdFormat.fullClassName) {
            typeId = type.getName();
        } else {
            throw new BeanException("Unsupported id format: " + idFormat);
        }
    }

    /**
     * A TypeDescriptor may be created for a base class and thus it's type/typeId
     * will not necessarily match every bean it represents. This method serves as a wrapper
     * to get the actual type for a type id. The return value will either be the value of {@link #type},
     * a subclass of {#type}, or null.
     *
     * @param actualTypeId
     * @see {@link #getActualTypeId(BeanDescriptor)}
     * @return
     */
    public Class<?> getActualType(String actualTypeId) {
        if(actualTypeId == null) {
            return null;
        }

        // if an exact match just return this type

        if(typeId.equals(actualTypeId)) {
            return type;
        }

        // package-based type ids won't usually have an exact match;
        // in this case we try to lookup a class by name and see if
        // it's a match for the current type info

        try {
            Class<?> actualType = Class.forName(actualTypeId);

            // the the found type is a subclass we found a match

            if(type.isAssignableFrom(actualType)) {
                return actualType;
            }
        } catch (ClassNotFoundException e) {
            return null;
        }

        return null;
    }

    /**
     * This is the reverse of {@link #getActualType(String)} to get the actual
     * type for a bean. However since the bean info is given as a param we know
     * exactly which type we are working with and will never turn null
     * @param beanInfo
     * @return
     */
    public String getActualTypeId(BeanDescriptor beanInfo) {
        // registering superclasses requires full class names so that's
        // the only condition we have to check here

        if(idFormat == IdFormat.fullClassName) {
            return beanInfo.cls.getName();
        }

        return typeId;
    }
}
