package org.sumatra.util.beans;

import java.lang.reflect.Field;

public class FieldDescriptor implements GetterDescriptor, SetterDescriptor {
    protected Field field;

    public FieldDescriptor(Field field) {
         this.field = field;
    }

    @Override
    public Class<?> getType() {
        return field.getType();
    }

    @Override
    public Object get(Object bean, Object... extraArgs) {
        try {
            return field.get(bean);
        } catch (IllegalAccessException e) {
            throw new BeanException(e);
        }
    }

    @Override
    public void set(Object bean, Object value, Object... extraArgs) {
        try {
            field.set(bean, value);
        } catch (IllegalAccessException e) {
            throw new BeanException(e);
        }
    }
}
