package org.sumatra.util.beans;

public class BeanUtils {
    private static final BeanManager manager = new BeanManager();

    public static BeanDescriptor getBeanInfo(Class<?> cls) {
        return manager.getBeanInfo(cls);
    }

    public static Object get(Object bean, String name, Object... extraArgs) {
        return manager.get(bean, name, extraArgs);
    }

    public static void set(Object bean, String name, Object value, Object... extraArgs) {
        manager.set(bean, name, value, extraArgs);
    }
}
