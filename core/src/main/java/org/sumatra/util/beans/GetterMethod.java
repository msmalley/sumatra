package org.sumatra.util.beans;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GetterMethod implements GetterDescriptor {
    protected Method method;

    public GetterMethod(Method method) {
        this.method = method;
    }

    @Override
    public Class<?> getType() {
        return method.getReturnType();
    }

    @Override
    public Object get(Object bean, Object... extraArgs) {
        int numArgs = method.getParameterTypes().length;
        Object[] args = new Object[numArgs];
        int numExtraArgs = extraArgs.length <= numArgs ? extraArgs.length : numArgs;

        System.arraycopy(extraArgs, 0, args, 0, numExtraArgs);

        try {
            return method.invoke(bean, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new BeanException(e);
        }
    }
}
