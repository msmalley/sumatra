package org.sumatra.util.beans;

public class PropertyNotFoundException extends BeanException {
    public PropertyNotFoundException(String message) {
        super(message);
    }
}
