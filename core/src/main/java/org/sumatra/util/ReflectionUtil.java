package org.sumatra.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReflectionUtil {
    public static <T> T instantiate(Class<T> type) {
        // FIXME: handle inner classes
        try {
            return type.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Collection createCollection(Class<?> collectionType, Class<? extends Collection> defaultType) {
        if(collectionType.equals(Object.class)) {
            return instantiate(defaultType);
        }

        if(collectionType.isAssignableFrom(List.class)) {
            return createList((Class<List>)collectionType);
        }

        if(collectionType.isAssignableFrom(Set.class)) {
            return createSet((Class<Set>) collectionType);
        }

        return null;
    }

    public static <T extends List> T createList(Class<T> type) {
        T instance;

        if(List.class.equals(type)) {
            // a generic list is requested, default to ArrayList
            instance = (T) new ArrayList<>();
        } else {
            // specific type is requested, try to instantiate it
            instance = (T) ReflectionUtil.instantiate((Class<List>) type);
        }

        return instance;
    }

    public static <T extends Set> T createSet(Class<T> type) {
        T instance;

        if(Set.class.equals(type)) {
            // a generic set is requested, default to HashSet
            instance = (T) new HashSet<>();
        } else {
            // specific type is requested, try to instantiate it
            instance = (T) ReflectionUtil.instantiate((Class<Set>) type);
        }

        return instance;
    }

    public static <T extends Map> T createMap(Class<T> type) {
        T instance;

        if(Map.class.equals(type)) {
            // a generic map is requested, default to HashMap
            instance = (T) new HashMap<>();
        } else {
            // specific type is requested, try to instantiate it
            instance = (T) ReflectionUtil.instantiate((Class<Map>) type);
        }

        return instance;
    }

    public static <T> T[] createArray(Class<T> type, int length) {
        return (T[]) Array.newInstance(type, length);
    }

    /**
     * Recursive method to find an annotation on a class, one of it's interfaces, or it's superclass
     * (in that order.)
     * @param cls
     * @param annotationType
     * @param <T>
     * @return
     */
    public static <T extends Annotation> T findAnnotation(Class<?> cls, Class<T> annotationType) {
        T anno = cls.getAnnotation(annotationType);

        if(anno != null) {
            return anno;
        }

        // try interfaces

        for(Class<?> iface : cls.getInterfaces()) {
            anno = findAnnotation(iface, annotationType);

            if(anno != null) {
                return anno;
            }
        }

        // try super class

        Class<?> superClass = cls.getSuperclass();

        if(superClass == null) {
            return null;
        }

        return findAnnotation(superClass, annotationType);
    }
}
