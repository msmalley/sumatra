package org.sumatra.exceptions;

public class BaseException extends RuntimeException {
    public int httpStatus;

    public BaseException(String message, int httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public BaseException(Throwable cause, int httpStatus) {
        super(cause);
        this.httpStatus = httpStatus;
    }

    public BaseException(String message, Throwable cause, int httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }
}
