package org.sumatra.exceptions;

import org.sumatra.util.HttpStatus;

public class ServerException extends BaseException {
    public ServerException(String message) {
        super(message, HttpStatus.InternalServerError);
    }

    public ServerException(Throwable cause) {
        super(cause, HttpStatus.InternalServerError);
    }

    public ServerException(String message, Throwable cause) {
        super(message, cause, HttpStatus.InternalServerError);
    }
}
