package org.sumatra.patch;

import org.sumatra.managers.SumatraContext;

public interface Patch {
    public void doPatch(SumatraContext ctx);
}
