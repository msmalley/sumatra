package org.sumatra.domain;

import javax.persistence.Column;
import javax.persistence.Id;

import org.sumatra.util.Dictionary;
import org.sumatra.util.JsonUtil;
import org.sumatra.util.json.ObjectMapper;

public class BaseObject {
    @Id
    public String id;

    @Column
    public String title;

    public Dictionary toDict() {
        return JsonUtil.getObjectMapper().convertValue(this, Dictionary.class);
    }
}
