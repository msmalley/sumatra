package org.sumatra.api;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.sumatra.domain.BaseObject;

@Path("/objects")
public interface ObjectsWebservice {
    @POST
    public BaseObject create(String title);
}
