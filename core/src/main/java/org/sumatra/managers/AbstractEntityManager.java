package org.sumatra.managers;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;

import org.sumatra.exceptions.ServerException;
import org.sumatra.util.Dictionary;

public abstract class AbstractEntityManager implements EntityManager {
    public Map<Class<?>, String> tableNames = new HashMap<>();

    public <T> T fromDict(Dictionary dict, Class<T> cls) {
        T instance = newInstance(cls);

        // FIXME: make recursive, support methods, etc

        for(String key : dict.keys()) {
            try {
                cls.getField(key).set(instance, dict.get(key, Object.class));
            } catch (Exception e) {
                throw new ServerException(e);
            }
        }

        return instance;
    }

    public <T> T fromMap(Map<String, Object> map, Class<T> cls) {
        return fromDict(new Dictionary(map), cls);
    }

    public <T> T newInstance(Class<T> cls) {
        // FIXME: inner classes
        try {
            return cls.newInstance();
        } catch (Exception e) {
            throw new ServerException(e);
        }
    }

    public String getStoreName(Class<?> type) {
        String tableName = tableNames.get(type);

        if(tableName != null) {
            return tableName;
        }

        Entity entity = type.getAnnotation(Entity.class);

        if(entity != null) {
            return entity.name();
        }

        return type.getSimpleName();
    }
}
