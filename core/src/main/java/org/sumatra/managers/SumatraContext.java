package org.sumatra.managers;

public interface SumatraContext {
    public String getDBHost();
    public int getDBPort();
    public String getDBName();
}
