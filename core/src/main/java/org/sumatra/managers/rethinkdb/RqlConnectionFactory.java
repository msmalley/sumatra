package org.sumatra.managers.rethinkdb;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import com.dkhenry.RethinkDB.RqlConnection;

public class RqlConnectionFactory implements PooledObjectFactory<RqlConnection> {
    protected String host;
    protected int port;

    public RqlConnectionFactory(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public PooledObject<RqlConnection> makeObject() throws Exception {
        return new DefaultPooledObject<>(RqlConnection.connect(host, port));
    }

    @Override
    public void destroyObject(PooledObject<RqlConnection> p) throws Exception {
        p.getObject().close();
    }

    @Override
    public boolean validateObject(PooledObject<RqlConnection> p) {
        return true;
    }

    @Override
    public void activateObject(PooledObject<RqlConnection> p) throws Exception {
        // pass
    }

    @Override
    public void passivateObject(PooledObject<RqlConnection> p) throws Exception {
        p.getObject().close();
    }
}
