package org.sumatra.managers.rethinkdb;

import java.util.ArrayList;
import java.util.List;

import org.sumatra.domain.BaseObject;
import org.sumatra.exceptions.ServerException;
import org.sumatra.managers.AbstractEntityManager;
import org.sumatra.managers.SumatraContext;
import org.sumatra.util.Dictionary;
import org.sumatra.util.iter.Iter;

import com.dkhenry.RethinkDB.RqlCursor;
import com.dkhenry.RethinkDB.RqlMethodQuery;
import com.dkhenry.RethinkDB.RqlObject;
import com.dkhenry.RethinkDB.RqlQuery;
import com.dkhenry.RethinkDB.RqlTopLevelQuery;
import com.dkhenry.RethinkDB.errors.RqlDriverException;

public class RethinkManager extends AbstractEntityManager {
    public static List<Object> buildSort(List<SortSpec> sort) {
        List<Object> list = new ArrayList<>();

        for(SortSpec spec : sort) {
            // FIXME: support descending
            list.add(spec.field);
        }

        return list;
    }

    public RqlTopLevelQuery.DB db(SumatraContext ctx, RethinkDB rql) {
        String dbName = ctx.getDBName();
        return rql.db(ctx.getDBName());
    }

    public RqlQuery.Table table(SumatraContext ctx, RethinkDB rql, String table) {
        return db(ctx, rql).table(table);
    }

    @Override
    public RethinkDB getConnection(SumatraContext ctx) {
        return RethinkDB.connect(ctx.getDBHost(), ctx.getDBPort());
    }

    @Override
    public <T extends BaseObject> T create(SumatraContext ctx, T object) {
        if(object == null) {
            return null;
        }

        RethinkDB rql = getConnection(ctx);
        Class<T> type = (Class<T>)object.getClass();
        RqlMethodQuery.Insert insert = table(ctx, rql, getStoreName(type)).insert(object.toDict().toMap());

        try {
            RqlCursor cursor = rql.connection.run(insert);

            if(cursor.hasNext()) {
                RqlObject result = cursor.next();
                return fromMap(result.getMap(), type);
            }

            return null;
        } catch (RqlDriverException e) {
            throw new ServerException(e);
        }
    }

    @Override
    public <T extends BaseObject> T save(SumatraContext ctx, T object) {
        if(object == null) {
            return null;
        }

        RethinkDB rql = getConnection(ctx);
        Class<T> type = (Class<T>)object.getClass();
        RqlMethodQuery.Update update = table(ctx, rql, getStoreName(type)).update(object.toDict().toMap());
        return executeAndReturnFirst(rql, update, type);
    }

    @Override
    public <T extends BaseObject> T get(SumatraContext ctx, Class<T> type, String id) {
        RethinkDB rql = getConnection(ctx);
        RqlMethodQuery.Get get = table(ctx, rql, getStoreName(type)).get(id);
        return executeAndReturnFirst(rql, get, type);
    }

    @Override
    public <T extends BaseObject> T first(SumatraContext ctx, Class<T> type, Dictionary spec) {
        RethinkDB rql = getConnection(ctx);
        RqlMethodQuery.Filter filter = table(ctx, rql, getStoreName(type)).filter(spec.toMap());
        return executeAndReturnFirst(rql, filter, type);
    }

    @Override
    public <T extends BaseObject> Iter<T> find(SumatraContext ctx, Class<T> type, Dictionary spec, List<SortSpec> sort, int limit, int offset) {
        RethinkDB rql = getConnection(ctx);
        RqlMethodQuery.Filter filter = table(ctx, rql, getStoreName(type))
                .order_by(buildSort(sort))
                .limit(limit)
                .skip(offset)
                .filter(spec.toMap());
        return execute(rql, filter, type);
    }

    @Override
    public <T extends BaseObject> void delete(SumatraContext ctx, Class<T> type, Dictionary spec) {
        RethinkDB rql = getConnection(ctx);
        RqlMethodQuery.Delete delete = table(ctx, rql, getStoreName(type)).delete(spec.toMap());
        execute(rql, delete, type);
    }

    public <T> Iter<T> execute(RethinkDB rql, RqlQuery query, Class<T> type) {
        try {
            RqlCursor cursor = rql.run(query);
            return new Iter<T>(new RqlCursorIterator(this, rql, cursor, type));
        } catch (RqlDriverException e) {
            throw new ServerException(e);
        }
    }

    public <T> T executeAndReturnFirst(RethinkDB rql, RqlQuery query, Class<T> type) {
        Iter<T> iter = execute(rql, query, type);

        if(iter.hasNext()) {
            return iter.next();
        }

        return null;
    }
}
