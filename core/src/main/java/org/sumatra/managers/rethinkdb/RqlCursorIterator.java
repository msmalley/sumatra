package org.sumatra.managers.rethinkdb;

import java.util.Iterator;

import org.sumatra.exceptions.ServerException;

import com.dkhenry.RethinkDB.RqlCursor;
import com.dkhenry.RethinkDB.errors.RqlDriverException;

public class RqlCursorIterator<T> implements Iterator<T> {
    private RethinkManager rethinkManager;
    protected RethinkDB rql;
    protected RqlCursor cursor;
    protected Class<T> cls;

    public RqlCursorIterator(RethinkManager rethinkManager, RethinkDB rql, RqlCursor cursor, Class<T> cls) {
        this.rethinkManager = rethinkManager;
        this.rql = rql;
        this.cursor = cursor;
        this.cls = cls;
    }

    @Override
    public boolean hasNext() {
        boolean hasNext = this.cursor.hasNext();
        if(! hasNext) {
            rql.close();
        }
        return hasNext;
    }

    @Override
    public T next() {
        try {
            return rethinkManager.fromMap(cursor.next().getMap(), cls);
        } catch (RqlDriverException e) {
            throw new ServerException(e);
        }
    }

    @Override
    public void remove() {
        this.cursor.remove();
    }
}
