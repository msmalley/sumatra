package org.sumatra.managers.rethinkdb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.sumatra.exceptions.ServerException;
import org.sumatra.util.Dictionary;
import org.sumatra.util.iter.Iter;
import org.sumatra.util.iter.IterMapper;

import com.dkhenry.RethinkDB.RqlConnection;
import com.dkhenry.RethinkDB.RqlCursor;
import com.dkhenry.RethinkDB.RqlObject;
import com.dkhenry.RethinkDB.RqlQuery;
import com.dkhenry.RethinkDB.RqlTopLevelQuery;
import com.dkhenry.RethinkDB.errors.RqlDriverException;

public class RethinkDB implements AutoCloseable {
    protected static Map<String, GenericObjectPool<RqlConnection>> connectionPools = new HashMap<>();

    public static RethinkDB connect(String host, int port) {
        String key = host + port;

        GenericObjectPool<RqlConnection> pool = connectionPools.get(key);

        if(pool == null) {
            pool = new GenericObjectPool<>(new RqlConnectionFactory(host, port));
            connectionPools.put(key, pool);
        }

        try {
            RqlConnection rql = pool.borrowObject();
            return new RethinkDB(pool, rql);
        } catch (Exception e) {
            throw new ServerException(e);
        }
    }

    protected GenericObjectPool<RqlConnection> pool;
    protected RqlConnection connection;

    public RethinkDB(GenericObjectPool<RqlConnection> pool, RqlConnection rql) {
        this.pool = pool;
        this.connection = rql;
    }

    public RqlConnection connection() {
        return connection;
    }

    public RqlTopLevelQuery.DB db(String name) {
        return connection.db(name);
    }

    public RqlTopLevelQuery.Table table(String db, String name) {
        return db(db).table(name);
    }

    public RqlCursor run(RqlQuery query) throws RqlDriverException {
        return connection.run(query);
    }

    @Override
    public void close() {
        pool.returnObject(connection);
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    public void tableCreate(String db, String table) throws RqlDriverException {
        run(connection().db(db).table_create(table));
    }

    public Iter<String> tableList(String db) throws RqlDriverException {
        RqlCursor cursor = run(connection().db(db).table_list());
        RqlObject result = cursor.next();
        return new Iter<>(result.getList()).map(new IterMapper<Object,String>() {
            @Override
            public String map(Object obj) {
                return (String)obj;
            }
        });
    }

    public void tableDrop(String db, String table) throws RqlDriverException {
        run(connection().db(db).table_drop(table));
    }

    public void insert(String db, String table, Map<String,Object> obj) throws RqlDriverException {
        run(table(db, table).insert(obj));
    }

    public Iter<Map<String,Object>> findObjects(String db, String table, Object...filters) throws RqlDriverException {
        RqlQuery query = table(db, table);

        if(filters.length > 0) {
            query.filter(filters);
        }

        RqlCursor cursor = run(query);
        return wrapAsObjects(cursor);
    }

    public static Iter<Map<String,Object>> wrapAsObjects(RqlCursor cursor) {
        return new Iter<>(cursor).map(new IterMapper<RqlObject, Map<String,Object>>() {
            @Override
            public Map<String,Object> map(RqlObject obj) {
                try {
                    return obj.getMap();
                } catch (RqlDriverException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
