package org.sumatra.managers;

import java.util.List;

import org.sumatra.domain.BaseObject;
import org.sumatra.util.Dictionary;
import org.sumatra.util.iter.Iter;

public interface EntityManager {
    public static class SortSpec {
        public String field;
        public boolean ascending;

        public static SortSpec sort(String field) {
            return new SortSpec(field, true);
        }

        public static SortSpec sort(String field, boolean ascending) {
            return new SortSpec(field, ascending);
        }

        public SortSpec(String field, boolean ascending) {
            this.field = field;
            this.ascending = ascending;
        }
    }

    public Object getConnection(SumatraContext ctx);
    public <T extends BaseObject> T create(SumatraContext ctx, T object);
    public <T extends BaseObject> T save(SumatraContext ctx, T object);
    public <T extends BaseObject> T get(SumatraContext ctx, Class<T> type, String id);
    public <T extends BaseObject> T first(SumatraContext ctx, Class<T> type, Dictionary spec);
    public <T extends BaseObject> Iter<T> find(SumatraContext ctx, Class<T> type, Dictionary spec, List<SortSpec> sort, int limit, int offset);
    public <T extends BaseObject> void delete(SumatraContext ctx, Class<T> type, Dictionary spec);
}
